var markerCluster;
var existingInfoDiv = [];
var existingMarkers = [];
var nOfExistingMarker = [];
var mapMarkers = [];

var customLabel = {
		Merimee : {
			label : 'M'
		},
		Palissy : {
			label : 'P'
		}
};

function initMap() {
	var map = new google.maps.Map(document.getElementById('map'), {
		center : new google.maps.LatLng(42.787624, 0.594529),
		zoom : 7
	});

	var infWin = new google.maps.InfoWindow;

	var xmlUrl = "MapsServlet?la1=90&lo1=180&la2=-90&lo2=-180";

	downloadUrl(xmlUrl, function(data) {
		var xml = data.responseXML;
		var markers = xml.documentElement.getElementsByTagName('marker');

		Array.prototype.forEach.call(markers,function(markerElem) {
			var name = markerElem.getElementsByTagName("knownAs")[0].childNodes[0].nodeValue;
			var type = markerElem.getAttribute('type');

			var description = "";
			if (markerElem.getElementsByTagName("description")[0].childNodes[0]) {
				description = markerElem.getElementsByTagName("description")[0].childNodes[0].nodeValue;
			}

			var point = new google.maps.LatLng(
					parseFloat(markerElem.getAttribute('lat')),
					parseFloat(markerElem.getAttribute('lng'))
			);

			var thumbnail = "";
			if (markerElem.getElementsByTagName("thumbnail")[0].childNodes[0]) {
				thumbnail = markerElem.getElementsByTagName("thumbnail")[0].childNodes[0].nodeValue;
			}

			var coarseLat = markerElem.getAttribute('lat').substring(0, 7);
			var coarseLng = markerElem.getAttribute('lng').substring(0, 7);

			var infoDiv;
			// If another marker exists in the vicinity, uses its info window and don't create another
			if (existingInfoDiv[coarseLat]
			&& existingInfoDiv[coarseLat][coarseLng]) {
				infoDiv = existingInfoDiv[coarseLat][coarseLng];

				nOfMarkers = nOfExistingMarker[coarseLat][coarseLng] + 1;
				nOfExistingMarker[coarseLat][coarseLng] = nOfMarkers;

				var existingMarker = existingMarkers[coarseLat][coarseLng];
				var label = {
						text : '' + nOfMarkers,
						color : 'white'
				};
				existingMarker.setLabel(label);
			} else {
				infoDiv = document.createElement('div');

				var icon = customLabel[type] || {};
				var marker = new google.maps.Marker({
					map : null,
					position : point,
					label : icon.label
				});
				marker.addListener('click', function() {
					imgs = infoDiv.getElementsByTagName("img");

					for (var i = 0; i < imgs.length; i++)
						imgs[i].setAttribute('src',imgs[i].getAttribute('data-source'));

					infWin.setContent(infoDiv);
					infWin.open(map,marker);
				});

				// Update information necessary for merging markers
				if (!existingInfoDiv[coarseLat]) {
					existingInfoDiv[coarseLat] = [];
					existingMarkers[coarseLat] = [];
					nOfExistingMarker[coarseLat] = [];
				}
				existingInfoDiv[coarseLat][coarseLng] = infoDiv;
				existingMarkers[coarseLat][coarseLng] = marker;
				nOfExistingMarker[coarseLat][coarseLng] = 1;

				mapMarkers.push(marker);
			}

			if (type == "Merimee")
				addInfo(infoDiv, name, description,thumbnail, false);
			else
				addInfo(infoDiv, name, description,thumbnail, true);
		});

		markerCluster = new MarkerClusterer(map, mapMarkers, {
			maxZoom : 20,
			imagePath : 'mapsclusterer/m'
		});
	});

}

function addInfo(infoWinContent, name, description, thumbnail, append) {
	var noticeContent = document.createElement('div');
	noticeContent.style.overflow = 'hidden';
	noticeContent.style.textAlign = 'justify';
	noticeContent.style.paddingRight = '10px';

	// Create winContent title
	var strong = document.createElement('strong');
	strong.textContent = name;
	strong.style.fontSize = "large";
	noticeContent.appendChild(strong);
	noticeContent.appendChild(document.createElement('br'));

	// Insert image in winContent if any exist
	if (thumbnail) {
		var img = document.createElement('img');
		img.setAttribute('src', 'blank.png');
		img.setAttribute('data-source', thumbnail);
		img.style.maxWidth = '200px';
		noticeContent.appendChild(img);
	}

	// Create winContent description
	var text = document.createElement('span');
	text.textContent = description;
	noticeContent.appendChild(text);

	if (append) {
		infoWinContent.appendChild(noticeContent);
		infoWinContent.appendChild(document.createElement('hr'));
	} else {
		infoWinContent.insertBefore(document.createElement('hr'),
				infoWinContent.firstChild);
		infoWinContent.insertBefore(noticeContent, infoWinContent.firstChild);
	}
}

function downloadUrl(url, callback) {
	var request = window.ActiveXObject ? new ActiveXObject('Microsoft.XMLHTTP')
	: new XMLHttpRequest;

	request.onreadystatechange = function() {
		if (request.readyState == 4) {
			request.onreadystatechange = function() {
			};
			callback(request, request.status);
		}
	};

	request.open('GET', url, true);
	request.send(null);
}