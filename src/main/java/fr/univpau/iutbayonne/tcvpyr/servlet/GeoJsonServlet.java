package fr.univpau.iutbayonne.tcvpyr.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpHost;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;

import com.vividsolutions.jts.io.geojson.GeoJsonWriter;

import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.reader.elastic.ElasticJsonReader;

/**
 * Servlet providing a JSON wto populate the map in 
 * notices.html with geometries of (ObjetEtude) 
 */
public class GeoJsonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GeoJsonServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RestClient restClient = RestClient.builder(new HttpHost("localhost", 9200, "http")).build();

		try {
			String elasticIndex = getServletContext().getInitParameter("elasticIndex");
			
			// Query Elasticsearch for a json containing the list of ObjetEtudes
			Response elasticResp = restClient.performRequest(
					"GET", 
					elasticIndex+"/_search?size=10000",
					Collections.emptyMap());

			InputStream inputStream = elasticResp.getEntity().getContent();

			String json = "";
			Scanner s2 = new Scanner(inputStream);
			while (s2.hasNext()) {
				json += s2.nextLine();
			}
			s2.close();

			// Parse json from Elasticsearch into a list of ObjetEtudes
			ElasticJsonReader eReader = new ElasticJsonReader(json);
			List<ObjetEtude> oeList = eReader.read();
			
			// Creates geojson
			String geoJson = "{\"type\":\"FeatureCollection\",\"features\": [\n";
			GeoJsonWriter geoJsonWriter = new GeoJsonWriter();
			for (ObjetEtude objetEtude : oeList) {
				geoJson += "{\"type\":\"Feature\",\"geometry\":" + geoJsonWriter.write(objetEtude.getAdresse().getLocalisation()) + "},";
			}
			geoJson = geoJson.substring(0, geoJson.length()-1) + "]}";
			
			// Writes geojson in the response
			response.setContentType("application/json");
			response.getWriter().print(geoJson);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			restClient.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
