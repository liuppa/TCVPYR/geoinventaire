package fr.univpau.iutbayonne.tcvpyr.servlet;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.elasticsearch.client.Response;
import org.elasticsearch.client.RestClient;
import org.w3c.dom.Document;

import fr.univpau.liuppa.tcvpyr.model.Illustration;
import fr.univpau.liuppa.tcvpyr.model.ObjetEtude;
import fr.univpau.liuppa.tcvpyr.reader.elastic.ElasticJsonReader;
import fr.univpau.liuppa.tcvpyr.writer.mapsxml.MapsXmlWriter;

/**
 * Servlet providing a XML to populate the map in 
 * notices.html with POIs of (ObjetEtude) 
 */
public class MapsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Default constructor.
	 */
	public MapsServlet() {
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      responsee)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String la1 = request.getParameter("la1");
		String lo1 = request.getParameter("lo1");
		String la2 = request.getParameter("la2");
		String lo2 = request.getParameter("lo2");

		RestClient restClient = RestClient.builder(new HttpHost("localhost", 9200, "http")).build();

		try {
			ServletContext context = getServletContext();
			InputStream is = context.getResourceAsStream("/WEB-INF/elastic-query/spatial_q_1.txt");
			
			// Create Elasticsearch query
			String jsonQuery = "";
			Scanner s1 = new Scanner(is);
			while(s1.hasNext()) {
				jsonQuery += s1.nextLine();
			}
			s1.close();

			jsonQuery = jsonQuery.replace("#la1", la1);
			jsonQuery = jsonQuery.replace("#lo1", lo1);
			jsonQuery = jsonQuery.replace("#la2", la2);
			jsonQuery = jsonQuery.replace("#lo2", lo2);
			
			String elasticIndex = getServletContext().getInitParameter("elasticIndex");
			
			// Query Elasticsearch for a json containing the list of ObjetEtudes
			HttpEntity entity = new NStringEntity(jsonQuery, ContentType.APPLICATION_JSON);
			Response elasticResp = restClient.performRequest(
					"GET", 
					elasticIndex+"/_search?size=10000",
					Collections.emptyMap(), entity);
			InputStream inputStream = elasticResp.getEntity().getContent();

			String json = "";
			Scanner s2 = new Scanner(inputStream);
			while (s2.hasNext()) {
				json += s2.nextLine();
			}
			s2.close();

			// Parse json from Elasticsearch into a list of ObjetEtudes
			ElasticJsonReader eReader = new ElasticJsonReader(json);
			List<ObjetEtude> oeList = eReader.read();
			
			String prefix = getServletContext().getInitParameter("imgDir");
			
			for (ObjetEtude objetEtude : oeList) {
				for (Illustration illu : objetEtude.getIllustrations()) {
					illu.setAccesImage(prefix + illu.getAccesImage());
					illu.setAccesMiniature(prefix + illu.getAccesMiniature());
				}
			}
			
			MapsXmlWriter mXmlWriter = new MapsXmlWriter(oeList, null);
			
			// Create XML and write it into the response
			response.setContentType("text/xml;charset=UTF-8");
			
			Document d = mXmlWriter.parseToXmlDoc();
			DOMSource domSource = new DOMSource(d);
			StreamResult result = new StreamResult(response.getWriter());
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			transformer.transform(domSource, result);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			restClient.close();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
